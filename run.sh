#!/bin/bash
set -e

function runMACS {
    ## Init Values
    local mySampleID="$1"
    local myTissue="$2"
    local myFactor="$3"
    local myCondition="$4"
    local myTreatment="$5"
    local myReplicate="$6"
    local myReads="$7"
    local myInput="$8"
    ## Create Workspace
    local myINDIR="/data/reads"
    local myOUTDIR="/data/output/macs2/${Tissue}_${Factor}_${Condition}_${Treatment}"
    mkdir -p $myOUTDIR
    ## run macs2
    echo "calling peaks on $myReads vs $myInput"
    /usr/local/bin/macs2 callpeak \
			 -t $myReads \
			 -c $myInput \
			 -f BED \
			 -n ${myOUTDIR}/${mySampleID}_rep${myReplicate}_vs_Input \
			 -g hs \
			 -p 1e-3 \
			 --to-large
#			 -B \
#			 --SPMR \
    ## Keep at most 100K peaks
    local myOUTFILE="${myOUTDIR}/${mySampleID}_rep${myReplicate}_vs_Input_peaks.narrowPeak"
    sort -k 8nr,8nr $myOUTFILE | head -n 100000 | gzip -c > ${myOUTFILE}.gz
}
export -f runMACS

MANIFEST=/data/manifest.csv
IFS=","

if [[ -e $MANIFEST ]] ; then
	echo "starting with macs2 peak calling ... "
else
	echo "directory does not contain manifest file"
	exit 1
fi

TOTALLINES=$(cat $MANIFEST | wc -l)
TOTALSAMPLES=$(cut -f1 -d',' $MANIFEST | sort -u | wc -l)

if [[ $TOTALSAMPLES -ne $TOTALLINES ]] ; then
    echo "not all sample names are unique... quitting"
    return 1
fi

cd /data
## Individual Replicates
sed '1d' $MANIFEST | while read SampleID Tissue Factor Condition Treatment Replicate Reads Input; do
    if [[ ${Reads: -3} == "bam" ]] ; then
	Reads=${Reads%.bam}
	Input=${Input%.bam}
    else
	if [[ ${Reads: -12} == ".tagAlign.gz" ]] ; then
	    Reads=${Reads%.tagAlign.gz}
	    Input=${Input%.tagAlign.gz}
	else
	    echo "files are neither bam nor tagAlign.gz"
	    return 1
	fi
    fi
    OUTDIR="/data/output/macs2/${Tissue}_${Factor}_${Condition}_${Treatment}"
    pReads1="${OUTDIR}/pseudoreps/${Reads}_pr1"
    pReads2="${OUTDIR}/pseudoreps/${Reads}_pr2"
    Suffix="tagAlign.gz"
    if [[ -e "${Reads}.${Suffix}" ]]; then
	runMACS ${SampleID} ${Tissue} ${Factor} ${Condition} ${Treatment} ${Replicate} "${Reads}.${Suffix}" "${Input}.${Suffix}"
	runMACS ${SampleID} ${Tissue} ${Factor} ${Condition} ${Treatment} "${Replicate}_pr1" "${pReads1}.${Suffix}" "${Input}.${Suffix}"
	runMACS ${SampleID} ${Tissue} ${Factor} ${Condition} ${Treatment} "${Replicate}_pr2" "${pReads2}.${Suffix}" "${Input}.${Suffix}"
	##pooled
	if [[ ${Replicate} -eq 1 ]] ; then
	    Replicate=0
	    PooledReads="${OUTDIR}/${Tissue}_${Factor}_${Condition}_${Treatment}_pooledreads"
	    echo "calling peaks on pooled reads for ${PooledReads}"
	    bPooledReads="$(basename ${PooledReads})"
	    ppReads1="${OUTDIR}/pseudoreps/${bPooledReads}_pr1"
	    ppReads2="${OUTDIR}/pseudoreps/${bPooledReads}_pr2"
	    runMACS "pooledreads" ${Tissue} ${Factor} ${Condition} ${Treatment} ${Replicate} "${PooledReads}.${Suffix}" "${Input}.${Suffix}"
	    runMACS "pooledreads" ${Tissue} ${Factor} ${Condition} ${Treatment} "${Replicate}_pr1" "${ppReads1}.${Suffix}" "${Input}.${Suffix}"
	    runMACS "pooledreads" ${Tissue} ${Factor} ${Condition} ${Treatment} "${Replicate}_pr2" "${ppReads2}.${Suffix}" "${Input}.${Suffix}"
	fi
    fi
done
