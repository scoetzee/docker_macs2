## What is this repository for
* Call peaks with macs2 to be input to the idr pipeline
  * 


## How to use
place a file - `manifest.csv` - in the same directory as your alignment files.

run by mounting this directory - containing input data - to /data
and by mounting a different directory - containing the location for output - to /data/output

